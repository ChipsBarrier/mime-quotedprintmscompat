#include "EXTERN.h"
#include "perl.h"
#include "XSUB.h"
#include "ppport.h"
#include "string.h"


MODULE = MIME::QuotedPrintMsCompat             PACKAGE = MIME::QuotedPrintMsCompat


SV*
decode_qp_ms_compat(sv)
    SV* sv
    PROTOTYPE: $

        PREINIT:
    STRLEN len;
    char *str = SvPVbyte(sv, len);
    char const* end = str + len;
    char *r;
    char *whitespace = 0;
    /* True, if we indeed did encounter an encoded LF at and of line: */
    int rfc_nl = 0;

        CODE:
    RETVAL = newSV(len ? len : 1);
        SvPOK_on(RETVAL);
        r = SvPVX(RETVAL);
    while (str < end) {
        if (*str == ' ' || *str == '\t') {
            if (!whitespace)
                whitespace = str;
            str++;
        }
        else if (*str == '\r' && (str + 1) < end && str[1] == '\n') {
            whitespace = 0;
            if( rfc_nl ) {
                /* We've already found an encoded LF.  Hence, we skip the
                 * original LF found in the document. */
                rfc_nl = 0;
                str += 2;
            }
            else {
                /* Otherwise, add the original LF found in the document: */
                *r++ = *str++;
                *r++ = *str++;
            }
        }
        else if (*str == '\n') {
            whitespace = 0;
            if( rfc_nl ) {
                /* We've already found encoded LF(s) */
                str++;
                rfc_nl = 0;
            }
            else {
                /* NOTE: Not canonical LF in MIME-messages! */
                *r++ = *str++;
            }
        }
        else {
            rfc_nl = 0;
            if (whitespace) {
                while (whitespace < str) {
                    *r++ = *whitespace++;
                }
                whitespace = 0;
            }
            if (*str == '=') {
                if ((str + 2) < end && isXDIGIT(str[1]) && isXDIGIT(str[2])) {
                    char buf[3];
                    str++;
                    buf[0] = *str++;
                    buf[1] = *str++;
                    buf[2] = '\0';
                    *r++ = (char)strtol(buf, 0, 16);
                    if( '0' == buf[0] && ('A' == buf[1] || 'D' == buf[1]) ) {
                        /* Encoded LF at hand. */
                        rfc_nl = 1;
                    }
                }
                else {
                    /* look for soft line break */
                    char *p = str + 1;
                    while (p < end && (*p == ' ' || *p == '\t'))
                        p++;
                    if (p < end && *p == '\n')
                        str = p + 1;
                    else if ((p + 1) < end && *p == '\r' && *(p + 1) == '\n')
                        str = p + 2;
                    else
                        *r++ = *str++; /* give up */
                }
            }
            else {
                *r++ = *str++;
            }
        }
    }
    if (whitespace) {
        while (whitespace < str) {
            *r++ = *whitespace++;
        }
    }
    *r = '\0';
    SvCUR_set(RETVAL, r - SvPVX(RETVAL));

        OUTPUT:
    RETVAL
