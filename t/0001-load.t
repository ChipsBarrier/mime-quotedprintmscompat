use strict;
use warnings;
use Test::More;

#
# Test the basic features of our module:
#  - Is it loadabel at all?
#  - Is it OUR module, i.e. the ms compatible one?
#  - CAN it encode_cp and decode_qp?
#

use_ok('MIME::QuotedPrintMsCompat');

is $MIME::QuotedPrintMsCompat::VERSION, '0.02', 'Seems to be our MIME::QuotedPrintMsCompat';

can_ok('MIME::QuotedPrintMsCompat', 'decode_qp'); # Proves only the *decode_qp pointer is defined...
can_ok('MIME::QuotedPrintMsCompat', 'encode_qp'); # Proves only the *encode_qp pointer is defined...

can_ok('MIME::QuotedPrintMsCompat', 'decode_qp_ms_compat');

my $qpp   = \&MIME::QuotedPrintMsCompat::decode;
my $qpmcp = \&MIME::QuotedPrintMsCompat::decode_qp_ms_compat;
is $qpp, $qpmcp, "$qpp (decode) points to $qpmcp (decode_ms_compat)";



done_testing();
