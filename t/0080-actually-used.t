use strict;
use Test::More;
use Test::Exception;
use File::Temp;
use File::Basename qw/dirname/;

#
# Testdata comes from /t/data/base64_vs_qp.eml.  That MIME-Mail is expected to
# hold pairs of Binary-Files.  Each pair consists of the same decoded content,
# differently encoded.  The proposed filenames are expected to differ in case
# only, where the uppercase name belongs to the base64-encoded part and the
# lowercase name to the quoted-printable encoded one.
#


use MIME::QuotedPrintMsCompat;
use MIME::Parser;
use MIME::Parser::Filer;
use Cwd 'abs_path';

my $DataDir = dirname( abs_path(__FILE__) ) . '/data';

my $CLEANUP_TEMP_DIRS = 1;


## Take parameters from the tester:
if( scalar @ARGV )
{
    for my $p ( @ARGV ) {
        if ( $p eq '--keep-temp-dirs' ) {
            $CLEANUP_TEMP_DIRS = 0;
            note("Tempdirs will survive this test run.");
        }
    }
}
else {
    note("Tempdirs will be lost.   Provide ':: --keep-temp-dirs' to make them survive.");
}

test_used_by_mime_parser_filer_into();


sub test_used_by_mime_parser_filer_into
{
    note("== RUNNING TEST  used_by_mime_parser_filer_into");

    my $tempdir = File::Temp->newdir(
        '0080-actually-used-XXXXXX',
        CLEANUP => $CLEANUP_TEMP_DIRS,
        );
    my  $dirname = $tempdir->dirname();
    note("Using Tempdir $dirname") unless $CLEANUP_TEMP_DIRS;

    my $eml     = "$DataDir/base64_vs_qp.eml";

    my $filer   = MIME::Parser::FileInto->new( $tempdir->dirname );
    $filer->ignore_filename(0);         # Default - might change in the future

    my $parser  = MIME::Parser->new();
    $parser->filer( $filer );
    $parser->ignore_errors( 0 );        # Default is fault tolerant
    $parser->parse_open($eml);

    my @files = glob("$dirname/*");
    ok scalar( @files >= 2 ), "Files extracted";

    # Find the pairs:
    my %pairs;
    for my $fname ( @files )
    {
        my @parts = split '_', $fname;
        my $type = pop @parts;          # _b64[.ext] or _qp[.ext]
        $type =~ s/\..*$//;             # Strip of extension
        next unless ( 'qp' eq $type || 'b64' eq $type );
        my $stem = join '_', @parts;
        $pairs{ lc($stem) }{$type} = $fname;
    }
    # note explain(\%pairs);
    ok scalar( %pairs ), "Pairs found";

    for my $stem ( sort keys %pairs )
    {
        my $qp_size  = -s $pairs{$stem}{qp};
        my $b64_size = -s $pairs{$stem}{b64};
        is $qp_size, $b64_size, "$stem - Decoded sizes same same: $qp_size";
    }

    note("== END OF TEST  used_by_mime_parser_filer_into");
}

ok 1, 'REMOVE ME';

done_testing();
