use strict;
use Test::More;

use File::Basename qw(dirname);


my $DataDir = dirname(__FILE__) . '/data';

use_ok('MIME::QuotedPrintMsCompat');
use MIME::QuotedPrint;

test_decode_qp_rfc2045();
test_decode_qp_ms_compat();


sub test_decode_qp_rfc2045
{
    note("== RUNNING TEST  decode_qp_rfc2045");

    my %tests = (
        'Hello World=0A' => "Hello World\n",
        'Hello World=0D=0A' => "Hello World\r\n",
        );

    for my $t (sort keys %tests)
    {
        my $expected = $tests{$t};
        is decode_qp($t), $expected, "$t";
    }

    note("== END OF TEST  decode_qp_rfc2045");
}

sub test_decode_qp_ms_compat
{
    note("== RUNNING TEST  decode_qp_ms_compat");

    my %tests = (
        "Hello=0AWorld\r\n" => "Hello\nWorld\r\n", # implicit_nl
        "Hello\r\nWorld=0A" => "Hello\r\nWorld\n", # implicit_explicit_nl
        "Hello=0D=0A\r\nWorld=0A\r\n" => "Hello\r\nWorld\n", # explicit_explicit_nl
        "Hello=0D=0A\nWorld=0A\n" => "Hello\r\nWorld\n", # explicit_explicit_nl_2
        );

    for my $t (sort keys %tests)
    {
        my $expected = $tests{$t};
        my $display  = $t;
        $display =~ s/\r/\\r/mg;
        $display =~ s/\n/\\n/mg;
        is decode_qp($t), $expected, "$display";
    }

    note("== END OF TEST  decode_qp_ms_compat");
}


done_testing();
