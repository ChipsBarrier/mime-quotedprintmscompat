package MIME::QuotedPrintMsCompat;

use strict;
use 5.020002;                           # Debian >= 8

our $VERSION = "0.02";

BEGIN {
    use MIME::QuotedPrint;
    use MIME::Decoder::QuotedPrint;
    no warnings 'redefine';

    *MIME::QuotedPrint::decode             = \&decode_qp_ms_compat;
    *MIME::Decoder::QuotedPrint::decode_it = \&decode_it_ms_compat;
}

use XSLoader ();
use parent 'Exporter';
__PACKAGE__->XSLoader::load($VERSION);

our @EXPORT = qw(decode_qp);

*decode    = \&decode_qp_ms_compat;
*decode_qp = \&decode_qp_ms_compat;


# The 'fix' for MIME::Decoder::QuotedPrint:
sub decode_it_ms_compat {
    my ($self, $in, $out) = @_;
    my $init = 0;
    my $badpdf = 0;

    # $out->print("USING QuotedPrintMsCompat::decode");
    # use Test::More;
    # my $i = 0;

    local $_;
    while (defined($_ = $in->getline))
    {

        # Decode everything with ms-compatible decode_qp():
        $out->print(decode($_));
        # my $line = $_;
        # $line =~ s/\r/\\r/g;
        # $line =~ s/\n/\\n/g;
        # ++$i;
        # note("$i: $line");
    }
    1;
}

1;
